from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField
from wtforms.validators import DataRequired, Length, EqualTo, Email

#For logging in
class LoginForm(FlaskForm):
    email = StringField(label='Email', validators=[DataRequired(), Email()])
    password = PasswordField(label='Password', validators=[DataRequired(), Length(min = 15, max = 30)])
    submit = SubmitField(label='Submit')

#For registration
#Putting in checks for better security for users
class RegistrationForm(FlaskForm):
    email = StringField(label='Email', validators=[DataRequired(), Email(), Length(min = 5, max = 50)])
    password = PasswordField(label='Password', validators=[DataRequired(), Length(min = 15, max = 30)]) 
    confirm_password = PasswordField(label='Confirm password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField(label='Submit')
