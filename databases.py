import sqlite3
from sqlite3 import Error
import sys

try:
    con2 = sqlite3.connect('./loggin.db', check_same_thread=False)
    c2 = con2.cursor()
    c2.execute('''CREATE TABLE IF NOT EXISTS users (
        email TEXT NOT NULL PRIMARY KEY,
        password TEXT NOT NULL);''')
except Error as e:
    print(e)
    sys.exit(1)

try:
    conn = sqlite3.connect('./tiny.db', check_same_thread=False)
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS messages (
        id integer PRIMARY KEY, 
        sender TEXT NOT NULL,
        message TEXT NOT NULL);''')
    c.execute('''CREATE TABLE IF NOT EXISTS announcements (
        id integer PRIMARY KEY, 
        author TEXT NOT NULL,
        text TEXT NOT NULL);''')
except Error as e:
    print(e)
    sys.exit(1)
