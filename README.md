# Marius Login

# Run stuff:

```
python app.py
pip install email_validator
pip install bcrypt
pip install is_safe_url
```

# Sources

https://www.youtube.com/watch?v=PrsmxWdthg0&ab_channel=CodingShiksha
https://www.youtube.com/watch?v=sndILKXxMsA&list=PLutwis6L8ml-fBrd5p3nhsZ7Y-wduKNPA&ab_channel=CodeJana

# commits:

```
git add .
git commit -m "Added"
git push
```

# Before refactoring and optimalization

The passwords are available in the users dictonary.
Which is not hashed just in plain text.

You can log in without the password, it only asks for username.

* '); DROP TABLE messages;--
* '); DROP TABLE messages; (also works)
* '); DROP TABLE announcements;--

Deletes the tables when you send a message with this content.

If you search a query with:

'; DROP TABLE messages;--

it will also dorp the table.

But in this program you dont have to use the "--" comment padding, To bypass the error.
You can delete the table with "'; DROP TABLE messages;".

This works on both "Search" and "Message" froms.

"ERROR: SQLError: unrecognized token: "'""

The Reason this works without the comment padding and still runs when it gets an error. Is because the try except handeling runs through the '); DROP TABLE messages; and then gets an error on ') on the end.

Used SQLMAP to automate the attck aswell.

# Changes

Added registration forms, login forms

Added so you can only see your own messages

Removed querys so they are not diplayed in the output when wrting or looking at messages

original = app.secret_key = 'mY s3kritz'
os.urandom() makes a random bytes string
app.secret_key = os.urandom(20)

Using sqlit3 because this is more safe apsw, because stacked queries are not allowed.

Added database for passwords and email

In register i do "{{form.hidden_tag()}}" to stop csrf register attacks.

Added @login_required for all the methods that is used while being logged in, you could do /search or any of the others to bypass the login. Also there is now sessions for each user.

pip install is_safe_url
so is_safe_url function works

Hashing passwords with salt 
Checking username and password



# ----QUESTIONS----

I used OSWAP ZAP, to test my application. I see i have some holes in my security, i did not get enough time to cover them all
due to the national guard... Hence why the late submission aswell.

# Threat model – who might attack the application? What can an attacker do? What damage could be done (in terms of confidentiality, integrity, availability)? Are there limits to what an attacker can do? Are there limits to what we can sensibly protect against?

Users could be comprimised, it is possible ot get access to the hashed password as of now, through bufferoverflow.
There might also be possible to get in through cross site attack and injection. All theese are on the top 10 OSWAP.

There is always the possiblity of an bruteforce attack to get access to a users messages, to protect against this you need to limit tries before you get locked out of the site. Or you need things like a two-factor verification.

It is really hard to protect against DDOS-attacks, which is the most low-level and maybe most common attack, which denys you access for a short period of time. 

There could also be malicious users who would want to gain information from other users.

# What are the main attack vectors for the application?

* I have an buffer overflow vulnerability which gives the hashed password with salt, but this is insanley long due tolimitationson    15 chars to create a password and due to the salt being value 10 aswell.

* Format string Error 

* Content Security Policy (CSP) for headers.
* Header missing
* X-content header missing

* Cookie without sameSite attribute

* Server leaks through HTTP

* SQL injection may be possible: This is deffinatly the worst one, i tohught i had fixed this hole, but clearly not. 
* It is possible to inject through the login page.

* There is also the possiblity of phising, with email attacks or in person.

All theese problems need to be fixed for this site to be secure.

# What should we do (or what have you done) to protect against attacks?

* I force users to make a password which is 15 chars long. 
* Since the password space is the most important
* I have hashed passwords in a database with salt 
* I use sqlite3 for a better security
* I use a secret session key which is a random byte string
* I use sessions to hinder illegal loggins
* I check password and email on login

# What is the access control model?

* The model in this application is based on mail and password.
* The password is hashed with salt and stored in a database

# How can you know that you security is good enough? (traceability)

* You need to log users behavior, and the site sessions.
* You need to test the security with tools like OWASP ZAP or Burpsuite
* Use other tools like Kali Linux packages to test, Think like a hacker
* Pentesting whitehat

