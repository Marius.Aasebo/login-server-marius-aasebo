import os
import bcrypt
import flask
from flask import flash, Flask, abort, request, send_from_directory, make_response, render_template, url_for, redirect, session, g
from json import dumps, loads
from base64 import b64decode
from sqlite3 import Error
from markupsafe import escape
import flask_login
from flask_login import login_required, login_user, logout_user
from forms import LoginForm, RegistrationForm
from pygments.formatters import HtmlFormatter
from databases import *
from functions import *
from is_safe_url import is_safe_url

# inject = "'; insert into messages (sender,message) values ('foo', 'bar');select '"
cssData = HtmlFormatter(nowrap=True).get_style_defs('.highlight')
# conn = None

# Set up app
app = Flask(__name__)
# The secret key enables storing encrypted session data in a cookie, os.urandom() makes a random bytes string
app.secret_key = os.urandom(20) 

login_manager = flask_login.LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"

# Class to store user info
# UserMixin provides us with an `id` field and the necessary
# methods (`is_authenticated`, `is_active`, `is_anonymous` and `get_id()`)
class User(flask_login.UserMixin):
    pass

# This method is called whenever the login manager needs to get
# the User object for a given user id
@login_manager.user_loader
def user_loader(user_id):
    if user_id not in getUsers():
        return
    
    user = User()
    user.id = user_id
    return user

@app.route("/")
@app.route("/home")
def home():
    return render_template("home.html", title="Home")

@app.route("/register", methods=["GET", "POST"])
def register():
    form= RegistrationForm()
    if form.validate_on_submit():
        email = form.email.data
        password = form.password.data.encode('utf-8')
        

        c2.execute(f"INSERT INTO users (email, password) VALUES (?,?)", (email, bcrypt.hashpw(password, bcrypt.gensalt(10))))
        con2.commit()
        return redirect(url_for("home"))
    return render_template("./register.html",form=form, title="register")

#allowed hosts for safe url
allowed_hosts =  ["http://127.0.0.1:5000", 
"127.0.0.1:5000",
"127.0.0.1:5000/index.html", 
"127.0.0.1:5000/search", 
"127.0.0.1:5000/index.html",
"127.0.0.1:5000/announcements"
]

@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        email = form.email.data
        password = form.password.data.encode('utf-8')
        
        dbPw = getPw(email)
        if email in getUsers() and bcrypt.checkpw(password, dbPw): 
            
            user = user_loader(email)
            login_user(user)

            session["user"] = email

            #is_safe_url should check if the url is safe for redirects.
            if not is_safe_url(url_for('index_html'), allowed_hosts, require_https=False):
                return abort(400)
                
            return flask.redirect(url_for('index_html'))
    return render_template('./login.html', form=form, title="login")


@login_required
@app.route("/logout")
def logout():
    session.pop("user", None)
    logout_user()
    return redirect(url_for("login"))

@login_required
@app.route('/index.html')
def index_html():
    if g.user:
        return send_from_directory(app.root_path, 'index.html', mimetype='text/html')
    else:
        return redirect(url_for("login"))

@login_required
@app.get('/search')
def search():
    if g.user:   
        stmt = f"SELECT message FROM messages WHERE sender = '{g.user}'"
        result = f"Query: {pygmentize(stmt)}\n"
        try:
            
            cur = c.execute(stmt)
            
            rows = cur.fetchall()
            result = 'Your messages:\n'
            
            for row in rows:
                result = f'{result}    {dumps(row)}\n'
            return result
        except Error as e:
            return (f'{result}ERROR: {e}', 500)
    else:
        return redirect(url_for("login"))

@login_required
@app.route('/send', methods=['POST','GET'])
def send():
    if g.user: 
        try:
            sender = request.args.get('sender') or request.form.get('sender')
            message = request.args.get('message') or request.args.get('message')
            if not sender or not message:
                return f'ERROR: missing sender or message'
            stmt = f"INSERT INTO messages (sender, message) values ('{sender}', '{message}');"
            result = f"Query: {pygmentize(stmt)}\n"
            c.execute(stmt)
            conn.commit()
            return f'{message}   ...Message sent from user: {sender}'
        except Error as e:
            return f'{message}ERROR: {e}'
    else:
        return redirect(url_for("login"))

@login_required
@app.get('/announcements')
def announcements():
    if g.user: 
        try:
            stmt = f"SELECT author,text FROM announcements;"
            cur = c.execute(stmt)
            conn.commit()
            anns = []
            for row in cur:
                anns.append({'sender':escape(row[0]), 'message':escape(row[1])})
            return {'data':anns}
        except Error as e:
            return {'error': f'{e}'}
    else:
        return redirect(url_for("login"))

#checks that user is in session before allowing user to ener site
#https://www.youtube.com/watch?v=PrsmxWdthg0&ab_channel=CodingShiksha
@app.before_request
def before_request():
    g.user = None 
    if "user" in session:
        g.user = session["user"]

#debug mode on/off
if __name__ == '__main__':
    app.run(debug=True)