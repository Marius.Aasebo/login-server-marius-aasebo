from databases import *
from pygments.filters import NameHighlightFilter, KeywordCaseFilter
from pygments import token;
from pygments.lexers import SqlLexer
from pygments import highlight
from pygments.formatters import HtmlFormatter
from threading import local

tls = local()

def pygmentize(text):
    if not hasattr(tls, 'formatter'):
        tls.formatter = HtmlFormatter(nowrap = True)
    if not hasattr(tls, 'lexer'):
        tls.lexer = SqlLexer()
        tls.lexer.add_filter(NameHighlightFilter(names=['GLOB'], tokentype=token.Keyword))
        tls.lexer.add_filter(NameHighlightFilter(names=['text'], tokentype=token.Name))
        tls.lexer.add_filter(KeywordCaseFilter(case='upper'))
    return f'<span class="highlight">{highlight(text, tls.lexer, tls.formatter)}</span>'

#Gets all users
def getUsers():
    query = """SELECT email FROM users"""
    c2.execute(query)
    emails = c2.fetchall()
    emailsClean = [i[0] for i in emails]
    return emailsClean

#Gets hashed pw without salt
def getPw(user):
    query = f"""SELECT password FROM users WHERE email = '{user}'"""
    c2.execute(query)
    emails = c2.fetchone()
    emails = str(emails)[3:-3]
    return emails.encode('utf-8')